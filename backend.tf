terraform {
  backend "s3" {
    bucket = "asg-terraform-assignment-01"
    key = "terraform-assignment-02/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "mbua-tf-state-locking"
  }
}


/*
when creating the Dynamp DB table, make sure to create partition key as "LockID".
*/ 
